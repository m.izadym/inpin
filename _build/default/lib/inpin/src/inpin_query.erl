-module(inpin_query).
%% API
-export([send/2,
  decode_responce/2]).

send(Query, Value) when is_binary(Query) ->
 Pid =connection(),
%%  Pid0 = linn:get_process(sql_pool),
%%  {ok, Pid} = inpin_sql_connection:get_connection_Id(Pid0),
  Res = mysql:query(Pid, Query, Value),
%%  exit(Pid, normal),
  Res.

decode_responce(Header, Result) ->
  [lists:zip(Header, Item) || Item <- Result].

connection() ->
  {ok, IP} = application:get_env(inpin, 'mysql.ip'),
  {ok, User} = application:get_env(inpin, 'mysql.user'),
  {ok, Password} = application:get_env(inpin, 'mysql.password'),
  {ok, Database} = application:get_env(inpin, 'mysql.database'),

  {ok, Pid} = mysql:start_link([{host, IP},
    {user, User},
    {password, Password},
    {database, Database}]),

  Pid.