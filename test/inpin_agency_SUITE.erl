-module(inpin_agency_SUITE).
-author("MohammadIzady").
-include("inpin.hrl").
-include_lib("common_test/include/ct.hrl").
%% Test server callbacks
-export([suite/0, all/0,
  init_per_suite/1, end_per_suite/1]).

%% Test cases
-export([create/1,create_1000/1 , update/1 , delete/1 ,agency_ads/1]).

suite() -> [].

init_per_suite(Config) ->
  application:ensure_started(inpin),

  Config.


end_per_suite(_Config) ->
  ok.

all() ->
  [
    create,
    create_1000,
    update,
    delete,
    agency_ads].

create(_Config) ->
  Id = erlang:system_time(),
  ok = inpin_service_agency:create(Id, "test", 0),
  {ok, {[{_,Id}|_]}} = inpin_service_agency:get_by_id(Id),
ok.

create_1000(_Config)->
  Start = erlang:system_time(millisecond),
  [begin
  Id = erlang:system_time(),
  ok = inpin_service_agency:create(Id, "test", 0),
  Id end ||_ <- lists:seq(1,1000)],
  End = erlang:system_time(millisecond),

  ct:print("Agency 1000 Insert => ~p ms" , [End-Start]),
  ok.



update(_Config) ->
  Id = erlang:system_time(),
  ok = inpin_service_agency:create(Id, "test", 0),
  ok = inpin_service_agency:update(Id, "test update"),
  ct:print("Item ~p" , [inpin_service_agency:get_by_id(Id)] ),
  ok.

delete(_Config) ->
  Id = erlang:system_time(),
  ok = inpin_service_agency:create(Id, "test", 0),
  ok = inpin_service_agency:delete(Id),
 %% ct:print("Item ~p" , [inpin_service_agency:get_by_id(Id)] ),
  ok.

agency_ads(_Config) ->
  A = erlang:system_time(),
  ok = inpin_service_agency:create(A, "test A", 0),

  B = erlang:system_time(),
  ok = inpin_service_agency:create(B, "test B", A),

  C = erlang:system_time(),
  ok = inpin_service_agency:create(C, "test C", B),

  A_Ads_1 = erlang:system_time(),
  ok = inpin_service_listing:create(A_Ads_1, "test A1", A , 0.0 , 0.0),

  A_Ads_2 = erlang:system_time(),
  ok = inpin_service_listing:create(A_Ads_2, "test A3", A , 0.0 , 0.0),

  A_Ads_3 = erlang:system_time(),
  ok = inpin_service_listing:create(A_Ads_3, "test A3", A , 0.0 , 0.0),

  B_Ads_1 = erlang:system_time(),
  ok = inpin_service_listing:create(B_Ads_1, "test B1", B , 0.0 , 0.0),

  B_Ads_2 = erlang:system_time(),
  ok = inpin_service_listing:create(B_Ads_2, "test B1", B , 0.0 , 0.0),

  C_Ads_1 = erlang:system_time(),
  ok = inpin_service_listing:create(C_Ads_1, "test C1", C , 0.0 , 0.0),


  {ok ,1 } = inpin_service_listing:get_count_agency(C),

  {ok ,3 } = inpin_service_listing:get_count_agency(B),

  {ok ,6 } = inpin_service_listing:get_count_agency(A),

  ok.



