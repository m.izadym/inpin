-module(inpin_web_handler).
-include("inpin.hrl").
%% API
-export([init/2,
  allowed_methods/2,
  content_types_accepted/2,response_ok_option/0,
  response_ok/0, response_ok/1, response_ok/2,
  response_error/1, response_not_exist/0, response_not_found/0]).

init(Req, State) ->
  {ResponseStatus, Body} = handle_request(Req),
  Header = #{<<"content-type">> => <<"application/json">>,
    <<"Access-Control-Allow-Origin">> => <<"*">>,
    <<"Access-Control-Allow-Methods">> => <<"GET, PATCH, POST, DELETE, OPTIONS">>,
    <<"access-control-allow-headers">>=> <<"Origin, X-Requested-With, Content-Type, Accept">>,
    <<"Allow">> => <<"*">>
  },
  Res = cowboy_req:reply(ResponseStatus, Header, Body, Req),
  {ok, Res, State}.

allowed_methods(Req, State) ->
  {[<<"GET">>, <<"POST">>, <<"PATCH">>, <<"DELETE">>, <<"OPTIONS">>], Req, State}.

content_types_accepted(Req, State) ->
  {[{<<"application">>, <<"json">>, []} , {<<"text">>, <<"html">>, '*'}], Req, State}.

response_ok_option()->{200 , <<"*">>}.

response_ok() -> {200, ""}.
response_ok(Body) ->
  Response = jiffy:encode({[{data, Body},
    {meta, {[]}}]}),
  {200, Response}.
response_ok(Body, Count) ->
  Response = jiffy:encode({[{data, Body},
    {meta, {[{count, Count}]}}]}),
  {200, Response}.
response_error(Body) -> error({bad_request, Body}).
response_not_exist() -> error({not_allowd, ""}).
response_not_found() -> error({not_found, ""}).


%%====================================================================
%% Internal functions
%%====================================================================
handle_request(Req) ->
  Method = maps:get(method, Req),
  Bindings = maps:get(bindings, Req),
  CtrlName = maps:get(ctrl, Bindings),
  Params0 = cowboy_req:parse_qs(Req),

  try
    Action = get_action(Method),
    Ctrl = get_controller(CtrlName),
    RouteDate = lists:delete({ctrl, CtrlName}, maps:to_list(Bindings)),

    Params = case Action of
               get -> Params0;
               delete -> Params0;
               options -> Params0;
               _ ->
                 {ok, Body, _} = cowboy_req:read_body(Req),
                 {Items} = jiffy:decode(Body),
                 lists:merge([Params0, Items])
             end,

    case erlang:function_exported(Ctrl, Action, 2) of
      true -> erlang:apply(Ctrl, Action, [RouteDate, Params]);
      false -> {405, ""}
    end
  catch
    error:{un_auturize, _Comment} -> {401, ""};
    error:{bad_request, _Comment} -> {400, ""};
    error:{not_found, _Comment} -> {404, ""};
    error:{not_allowd, _Comment} -> {405, ""};
    T:E ->
      io:format("request ~p  Type ~p ,Error ~p", [Req, T, E]),
      {500, ""}
  end.


get_action(<<"GET">>) -> get;
get_action(<<"POST">>) -> post;
get_action(<<"PATCH">>) -> patch;
get_action(<<"DELETE">>) -> delete;
get_action(<<"OPTIONS">>) -> options.

get_controller(<<"agency">>) -> inpin_web_agency_handler;
get_controller(<<"listing">>) -> inpin_web_listing_handler;
get_controller(Else) -> error({not_allowd, Else}).
