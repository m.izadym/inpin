.PHONY: all test clean
REBAR=./resource/rebar3

all: compile

compile:
	${REBAR} compile

test:
	${REBAR} ct -v

clean:
	${REBAR} clean ;rm -rf _build/default/rel/*

build:
	${REBAR} release

run:
	_build/default/rel/inpin/bin/inpin console

tar:
	${REBAR} tar;cp _build/default/rel/inpin/inpin-1.0.0.tar.gz release
