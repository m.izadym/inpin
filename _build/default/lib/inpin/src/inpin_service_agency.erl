-module(inpin_service_agency).

%% API
-export([get_count/0,
  get_all/3,
  get_by_id/1,
  create/3,
  delete/1,
  update/2
]).

-spec get_count() -> {ok, integer()}|error.
get_count() ->
  QueryCount = <<"SELECT 'count',COUNT(*)as count FROM `agency` ">>,
  case inpin_query:send(QueryCount, []) of
    {ok, _, [[<<"count">>, Count]]} -> {ok, Count};
    _ -> error
  end.

get_by_id(Id) ->
  Query = <<"SELECT * FROM `agency` where id = ?">>,
  case inpin_query:send(Query, [Id]) of
    {ok, Head, Result} ->

      Items = inpin_query:decode_responce(Head, Result),
      Agencies = [
        {[{id, inpin_utils:find_default_int(Item, <<"id">>)},
          {name, inpin_utils:find_default_binary(Item, <<"name">>)},
          {parentId, inpin_utils:find_default_int(Item, <<"parent_id">>)}]} || Item <- Items],

    case length(Agencies) >0 of
      true ->{ok, lists:last(Agencies)};
      false -> error
    end;
    {error, _E} -> error
  end.

get_all(Sort, Limit, Page) ->
  Query = <<"SELECT * FROM `agency` ORDER BY ", Sort/binary, " DESC LIMIT ? OFFSET ?">>,

  case inpin_query:send(Query, [Limit, (Page * Limit)]) of
    {ok, Head, Result} ->

      Items = inpin_query:decode_responce(Head, Result),
      Agencies = [
        {[{id, inpin_utils:find_default_int(Item, <<"id">>)},
          {name, inpin_utils:find_default_binary(Item, <<"name">>)},
          {parentId, inpin_utils:find_default_int(Item, <<"parent_id">>)}]} || Item <- Items],
      {ok, Agencies};
    {error, _E} -> error
  end.

create(NewID, Name, ParentId) ->
  if ParentId == 0 ->
    Query = <<"INSERT INTO `agency`(id ,name) VALUES (?,?);">>,
    case inpin_query:send(Query, [NewID, Name]) of
      ok -> ok;
      {error, _E} -> error
    end;
    true ->
      Query = <<"INSERT INTO `agency`(id, name,parent_id) VALUES (?,?,?);">>,

      case inpin_query:send(Query, [NewID, Name, ParentId]) of
        ok ->
          %% get parents
          {ok, [<<"agency_id">>], Ides0} = inpin_query:send(<<"select DISTINCT agency_id from `agency_children` where childeren_id = ?;">>, [ParentId]),
          Ides = lists:merge(Ides0),

          Count = length(Ides)+ 1,
          QMark0 = lists:concat((lists:join("," ,["(?,?)"|| _ <-lists:seq(1,Count)]))),
          QMark = list_to_binary(QMark0),
          Values0 = lists:join(NewID ,  [ParentId|Ides]),
          Values = lists:append(Values0 , [NewID]),
          inpin_query:send(<<"INSERT INTO `agency_children`(agency_id,childeren_id) VALUES ",QMark/binary, " ;">>,Values),

          ok;
        {error, _E} -> error

      end
  end.

delete(Id)->
  Query = <<"DELETE FROM `agency` where id = ?;">>,
  case inpin_query:send(Query, [Id]) of
    ok ->
      inpin_query:send(<<"DELETE FROM `agency_children` where childeren_id = ?">>, [Id]),
      ok ;
    {error, _E} -> error
  end.

update(Id, Name )->
  Query = <<"UPDATE `agency` SET name = ? where id = ?">> ,
  case inpin_query:send(Query, [ Name ,Id]) of
    ok-> ok;
    {error, _E} -> error
  end.

%%%%====================================================================
%%%% Internal functions
%%%%====================================================================
