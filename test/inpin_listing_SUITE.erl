-module(inpin_listing_SUITE).
-author("MohammadIzady").
-include("inpin.hrl").
-include_lib("common_test/include/ct.hrl").
%% Test server callbacks
-export([suite/0, all/0,
  init_per_suite/1, end_per_suite/1]).

%% Test cases
-export([create/1, create_1000/1, update/1, delete/1]).

-export([location_search/1,location_search_1000/1]).
suite() -> [].

init_per_suite(Config) ->
  application:ensure_started(inpin),
  Id = erlang:system_time(),
  ok = inpin_service_agency:create(Id, "test", 0),

  [{agency_id, Id} | Config].


end_per_suite(_Config) ->
  ok.

all() ->
  [
    create,
    create_1000,
    update,
    delete,
    location_search,
    location_search_1000
  ].

create(Config) ->
  AgencyId = ?config(agency_id, Config),
  Id = erlang:system_time(),
  ok = inpin_service_listing:create(Id, "test", AgencyId, 0.0, 0.0),
  ok.

create_1000(Config) ->
  AgencyId = ?config(agency_id, Config),

  Start = erlang:system_time(millisecond),
  [begin
     Id = erlang:system_time(),
     ok = inpin_service_listing:create(Id, "test", AgencyId, 0.0, 0.0),
     Id end || _ <- lists:seq(1, 1000)],
  End = erlang:system_time(millisecond),

  ct:print("Listing 1000 Insert => ~p ms", [End - Start]).

update(Config) ->
  AgencyId = ?config(agency_id, Config),
  Id = erlang:system_time(),
  ok = inpin_service_listing:create(Id, "test", AgencyId, 0.0, 0.0),
  ok = inpin_service_listing:update(Id, "test update", 1.1, 1.1),
  ct:print("Item ~p", [inpin_service_listing:get_by_id(Id)]),
  ok.

delete(Config) ->
  AgencyId = ?config(agency_id, Config),
  Id = erlang:system_time(),
  ok = inpin_service_listing:create(Id, "test", AgencyId, 0.0, 0.0),
  ok = inpin_service_listing:delete(Id),
  %% ct:print("Item ~p" , [inpin_service_agency:get_by_id(Id)] ),
  ok.

location_search(Config) ->
  AgencyId = ?config(agency_id, Config),
  Id = erlang:system_time(),
  ok = inpin_service_listing:create(Id, "test", AgencyId, 10.0, -10.0),

  {ok, _, _} = inpin_service_listing:location_search(10.1, -10.0,10000 , 0, 0),
  ok.

location_search_1000(Config) ->
  AgencyId = ?config(agency_id, Config),
  Id = erlang:system_time(),

  ok = inpin_service_listing:create(Id, "test", AgencyId, 40.0, -40.0),

  Start = erlang:system_time(millisecond),
  [begin
  {ok, _, _} = inpin_service_listing:location_search(40.1, -40.0,10000 , 0, 0)
  end||
    _ <- lists:seq(1,1000)],
  End = erlang:system_time(millisecond),

  ct:print("Listing 1000 Location Search => ~p ms", [End - Start]).