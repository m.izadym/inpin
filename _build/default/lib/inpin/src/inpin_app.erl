%%%-------------------------------------------------------------------
%% @doc inpin public API
%% @end
%%%-------------------------------------------------------------------

-module(inpin_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
  application:ensure_started(linn),
 %% application:ensure_started(lager),
  application:ensure_started(mysql),
  application:ensure_started(cowboy),

%%  initial(),

  %% config web server
  {ok, PortWeb} = application:get_env(inpin,'web.server.port'),
  Dispatch = cowboy_router:compile(
    [{'_', [{"/api/v1/:ctrl[/:id]", inpin_web_handler, []}]}]),
  {ok, _} = cowboy:start_clear(http, [{port, PortWeb}], #{env => #{dispatch => Dispatch}}),


  inpin_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
  ok.

%%====================================================================
%% Internal functions
%%====================================================================

initial()->
  {ok, Count} = application:get_env(inpin, 'mysql.connection.count'),

  linn:add_pool(sql_pool, inpin_sql_connection, start, Count).
