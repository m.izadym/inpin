-module(inpin_service_listing).
-include("inpin_es_query.hrl").
%% API
-export([get_count/0,
  get_count_agency/1,
  get_all/3,
  get_by_id/1,
  get_by_agency_id/4,
  create/5,
  delete/1,
  update/4,
  location_search/5]).

get_count() ->
  QueryCount = <<"SELECT 'count',COUNT(*)as count FROM `listing` ">>,
  case inpin_query:send(QueryCount, []) of
    {ok, _, [[<<"count">>, Count]]} -> {ok, Count};
    _ -> error
  end.

get_count_agency(Id) ->
  QueryCount = <<"SELECT 'count',COUNT(*)as count FROM `listing` where  agency_id in ",
    "(SELECT childeren_id FROM `agency_children` where agency_id = ?) or agency_id = ?">>,
  case inpin_query:send(QueryCount, [Id, Id]) of
    {ok, _, [[<<"count">>, Count]]} -> {ok, Count};
    _ -> error
  end.

get_by_id(Id) ->
  Query = <<"SELECT * FROM `listing` where id = ?">>,
  case inpin_query:send(Query, [Id]) of
    {ok, Head, Result} ->
      Items = inpin_query:decode_responce(Head, Result),
      Listing = [
        {[{id, inpin_utils:find_default_int(Item, <<"id">>)},
          {name, inpin_utils:find_default_binary(Item, <<"name">>)},
          {agencyId, inpin_utils:find_default_int(Item, <<"agency_id">>)},
          {latitude, inpin_utils:find_default_int(Item, <<"latitude">>)},
          {longitude, inpin_utils:find_default_int(Item, <<"longitude">>)}]} || Item <- Items],

      case length(Listing) > 0 of
        true -> {ok, lists:last(Listing)};
        false -> error
      end;

    _ -> error
  end.

get_by_agency_id(Id, Sort, Limit, Page) ->
  Query = <<" SELECT * FROM `listing` WHERE agency_id in ",
    "(SELECT childeren_id FROM `agency_children` where agency_id = ?) or agency_id = ?",
    " ORDER BY ", Sort/binary, " DESC LIMIT ? OFFSET ? ;">>,
  case inpin_query:send(Query, [Id, Id, Limit, (Page * Limit)]) of
    {ok, Head, Result} ->
      Items = inpin_query:decode_responce(Head, Result),
      Listing = [
        {[{id, inpin_utils:find_default_int(Item, <<"id">>)},
          {name, inpin_utils:find_default_binary(Item, <<"name">>)},
          {agencyId, inpin_utils:find_default_int(Item, <<"agency_id">>)},
          {latitude, inpin_utils:find_default_int(Item, <<"latitude">>)},
          {longitude, inpin_utils:find_default_int(Item, <<"longitude">>)}]} || Item <- Items],
      {ok, Listing};

    _ -> error
  end.

get_all(Sort, Limit, Page) ->
  Query = <<"SELECT * FROM `listing` ORDER BY ", Sort/binary, " DESC LIMIT ? OFFSET ?">>,
  case inpin_query:send(Query, [Limit, (Page * Limit)]) of
    {ok, Head, Result} ->
      Items = inpin_query:decode_responce(Head, Result),
      Listing = [
        {[{id, inpin_utils:find_default_int(Item, <<"id">>)},
          {name, inpin_utils:find_default_binary(Item, <<"name">>)},
          {agencyId, inpin_utils:find_default_int(Item, <<"agency_id">>)},
          {latitude, inpin_utils:find_default_int(Item, <<"latitude">>)},
          {longitude, inpin_utils:find_default_int(Item, <<"longitude">>)}]} || Item <- Items],
      {ok, Listing};

    _ -> error
  end.

create(NewID, Name, AgencyId, Latitude, Longitude) ->
  Query = <<"INSERT INTO `listing`(id,name, agency_id, latitude, longitude) VALUES (?,?,?,?,?);">>,
  case inpin_query:send(Query, [NewID, Name, AgencyId, Latitude, Longitude]) of
    ok -> ok;
    {error, _E} -> error
  end.

delete(Id) ->
  Query = <<"DELETE FROM `listing` where id = ?;">>,
  case inpin_query:send(Query, [Id]) of
    ok -> ok;
    {error, _E} -> error
  end.

update(Id, Name, Latitude, Longitude) ->
  Query = <<"UPDATE listing SET name = ?, latitude = ? ,  longitude = ? where id = ?">>,
  case inpin_query:send(Query, [Name, Latitude, Longitude, Id]) of
    ok -> ok;
    {error, _E} -> error
  end.

location_search(Latitude, Longitude, Distance,Limit, Page)->
  Lat = erlang:float_to_binary(Latitude, [{decimals, 6}]),
  Lon = erlang:float_to_binary(Longitude, [{decimals, 6}]),

  LQ= get_location_query(Lat ,Lon),
  QueryCount = <<"SELECT 'count',COUNT(*)as count FROM ",LQ/binary>>,
  case inpin_query:send(QueryCount, [Distance]) of
    {ok, _, [[<<"count">>, Count]]} ->

  SQ = <<"SELECT * From ", LQ/binary ,"  ORDER BY t.distance ASC LIMIT ? OFFSET ?">>,
  case inpin_query:send(SQ, [Distance,Limit, (Page * Limit)]) of
    {ok, Head, Result} ->
      Items = inpin_query:decode_responce(Head, Result),
      Listing = [
        {[{id, inpin_utils:find_default_int(Item, <<"id">>)},
          {name, inpin_utils:find_default_binary(Item, <<"name">>)},
          {agencyId, inpin_utils:find_default_int(Item, <<"agency_id">>)},
          {latitude, inpin_utils:find_default_int(Item, <<"latitude">>)},
          {longitude, inpin_utils:find_default_int(Item, <<"longitude">>)},
          {distance ,inpin_utils:find_default_int(Item, <<"distance">>)}]} || Item <- Items],
      {ok, Listing ,Count };

    _ -> error
  end;
    _ -> error
  end.

%%====================================================================
%% Internal functions
%%====================================================================

get_location_query(Lat,Lon)->
  <<"( SELECT *,(6371 * acos(cos( radians(",Lat/binary,") )
        * cos( radians( latitude ) )
        * cos( radians( longitude ) - radians(",Lon/binary,") )
        + sin( radians(",Lat/binary,") )
        * sin( radians( latitude ) )
    ) ) as distance from listing ) as t where t.distance  <= ?">>.