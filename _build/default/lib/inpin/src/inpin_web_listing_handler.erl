-module(inpin_web_listing_handler).
-include("inpin.hrl").

%% API
-export([get/2, post/2, patch/2, delete/2, options/2]).

get([], [{<<"agency">>, _} | _] = Params) ->
  Page = inpin_utils:find_default_int(Params, <<"page">>, ?DEFAULT_SKIP),
  Limit = inpin_utils:find_default_int(Params, <<"limit">>, ?DEFAULT_LIMIT),
  Sort = inpin_utils:find_default_binary(Params, <<"sort">>, <<"id">>),
  AgencyId = inpin_utils:find_default_int(Params, <<"agency">>, 0),

  case inpin_service_listing:get_count_agency(AgencyId) of
    {ok, Count} ->
      case inpin_service_listing:get_by_agency_id(AgencyId, Sort, Limit, Page) of
        {ok, Res} ->
          inpin_web_handler:response_ok(Res, Count);
        error -> inpin_web_handler:response_error("db error")
      end;
    error -> inpin_web_handler:response_error("db error")
  end;

get([], Params) ->

  Page = inpin_utils:find_default_int(Params, <<"page">>, ?DEFAULT_SKIP),
  Limit = inpin_utils:find_default_int(Params, <<"limit">>, ?DEFAULT_LIMIT),
  Sort = inpin_utils:find_default_binary(Params, <<"sort">>, <<"id">>),
  Distance = inpin_utils:find_default_int(Params, <<"distance">>, ?DEFAULT_DISTANCE),
  LON = inpin_utils:find_default_float(Params, <<"lon">>, ?DEFAULT_LON),
  LAT = inpin_utils:find_default_float(Params, <<"lat">>, ?DEFAULT_LAT),

  if Distance =/= ?DEFAULT_DISTANCE ->
    case inpin_service_listing:location_search(LAT, LON, Distance, Limit, Page) of
      {ok, Res, Count} ->

        inpin_web_handler:response_ok(Res, Count);
      error -> inpin_web_handler:response_error("db error")
    end;
    true ->
      %% No distance query
      case inpin_service_listing:get_count() of
        {ok, Count} ->
          case inpin_service_listing:get_all(Sort, Limit, Page) of
            {ok, Items} -> inpin_web_handler:response_ok(Items, Count);
            error ->
              inpin_web_handler:response_error("db error")
          end;
        error ->
          inpin_web_handler:response_error("db error")
      end
  end;

get([{id, _}] = RouteData, _) ->
  Id = inpin_utils:find_default_int(RouteData, id, 0),
  case inpin_service_listing:get_by_id(Id) of
    {ok, Item} -> inpin_web_handler:response_ok(Item);
    error ->
      inpin_web_handler:response_not_found()
  end;
get(_, _) -> inpin_web_handler:response_not_exist().

post(_, Params) ->
  NewID = erlang:system_time(microsecond),
  Name = inpin_utils:find_default_binary(Params, <<"name">>, <<"">>),
  AgencyId = inpin_utils:find_default_int(Params, <<"agencyId">>, 0),
  Latitude = inpin_utils:find_default_int(Params, <<"latitude">>, 0.0),
  Longitude = inpin_utils:find_default_int(Params, <<"longitude">>, 0.0),

  case inpin_service_listing:create(NewID, Name, AgencyId, Latitude, Longitude) of
    ok -> inpin_web_handler:response_ok();
    error -> inpin_web_handler:response_error("db error")
  end.

patch([{id, _}] = RouteData, Params) ->
  Id = inpin_utils:find_default_int(RouteData, id, 0.0),
  Name = inpin_utils:find_default_binary(Params, <<"name">>, <<"">>),
  Latitude = inpin_utils:find_default_int(Params, <<"latitude">>, 0.0),
  Longitude = inpin_utils:find_default_int(Params, <<"longitude">>, 0.0),

  Exist = inpin_service_listing:get_by_id(Id),

  if Exist == error -> inpin_web_handler:response_not_found();
    true ->
      case inpin_service_listing:update(Id, Name, Latitude, Longitude) of
        ok -> inpin_web_handler:response_ok();
        error -> inpin_web_handler:response_error("db error")
      end
  end;

patch(_, _) -> inpin_web_handler:response_not_exist().

delete([{id, _Id0}] = Params, _) ->
  Id = inpin_utils:find_default_int(Params, id),

  Exist = inpin_service_listing:get_by_id(Id),

  if Exist == error -> inpin_web_handler:response_not_found();
    true ->
  case inpin_service_listing:delete(Id) of
    ok -> inpin_web_handler:response_ok();
    error -> inpin_web_handler:response_error("db error")
  end
  end;

delete(T, _) -> io:format("~n ~p", [T]), inpin_web_handler:response_not_exist().

options(_, _) -> inpin_web_handler:response_ok_option().
%%====================================================================
%% Internal functions
%%====================================================================





