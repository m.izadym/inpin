-module(inpin_web_agency_handler).
-include("inpin.hrl").

%% API
-export([get/2, post/2, patch/2, delete/2, options/2]).

get([], Params) ->
  Page = inpin_utils:find_default_int(Params, <<"page">>, ?DEFAULT_SKIP),
  Limit = inpin_utils:find_default_int(Params, <<"limit">>, ?DEFAULT_LIMIT),
  Sort = inpin_utils:find_default_binary(Params, <<"sort">>, <<"id">>),

  case inpin_service_agency:get_count() of
    {ok, Count} ->
      case inpin_service_agency:get_all(Sort, Limit, Page) of
        {ok, Items} -> inpin_web_handler:response_ok(Items, Count);
        error ->
          inpin_web_handler:response_error("db error")
      end;
    error ->
      inpin_web_handler:response_error("db error")
  end;

get([{id, _}] = RouteData, _) ->
  Id = inpin_utils:find_default_int(RouteData, id, 0),
  case inpin_service_agency:get_by_id(Id) of
    {ok, Item} -> inpin_web_handler:response_ok(Item);
    error ->
      inpin_web_handler:response_not_found()
  end;

get(_, _) -> inpin_web_handler:response_not_exist().

post(_, Params) ->
  NewID = erlang:system_time(microsecond),
  Name = inpin_utils:find_default_binary(Params, <<"name">>, <<"">>),
  ParentId = inpin_utils:find_default_int(Params, <<"parentId">>, 0),

  case inpin_service_agency:create(NewID, Name, ParentId) of
    ok -> inpin_web_handler:response_ok();
    error -> inpin_web_handler:response_error("db error")
  end;

post(_, _) -> inpin_web_handler:response_not_exist().

patch([{id, _}] = RouteData, Params) ->
  Id = inpin_utils:find_default_int(RouteData, id, 0),
  Name = inpin_utils:find_default_binary(Params, <<"name">>, <<"">>),

  Exist = inpin_service_agency:get_by_id(Id),

  if Exist == error -> inpin_web_handler:response_not_found();
    true ->
      case inpin_service_agency:update(Id, Name) of
        ok -> inpin_web_handler:response_ok();
        error -> inpin_web_handler:response_error("db error")
      end
  end;

patch(_, _) -> inpin_web_handler:response_not_exist().

delete([{id, _Id0}] = Params, _) ->

  Id = inpin_utils:find_default_int(Params, id),

  Exist = inpin_service_agency:get_by_id(Id),

  if Exist == error -> inpin_web_handler:response_not_found();
    true ->
      case inpin_service_agency:delete(Id) of
        ok -> inpin_web_handler:response_ok();
        error -> inpin_web_handler:response_error("db error")
      end
  end;

delete(_, _) -> inpin_web_handler:response_not_exist().

options(_, _) -> inpin_web_handler:response_ok_option().

%%====================================================================
%% Internal functions
%%====================================================================