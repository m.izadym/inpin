-module(inpin_utils).

%% API
-export([create_id/2,
  get_type_id/1,
  find_default_binary/2,
  find_default_binary/3,
  find_default_int/2,
  find_default_int/3,
  find_default_float/2,
  find_default_float/3,
  find_default_list/2,
  find_default_list/3,
  reform_local_date/1]).

create_id(Type, Id) ->
  iolist_to_binary([Type, "#", Id]).

get_type_id(Id) ->
  case binary:split(Id, <<"#">>) of
    [Type, Id] -> {Type, Id};
    _ -> error({Id, wrong_format})
  end.

find_default_binary(List, Key) ->
  case lists:keyfind(Key, 1, List) of
    {Key, Val} when is_binary(Val) -> Val;
    {Key, Val} when is_integer(Val) -> integer_to_binary(Val);
    _ -> <<"">>
  end.

find_default_binary(List, Key, Default) ->
  case find_default_binary(List, Key) of
    <<"">> -> Default;
    T -> T
  end.

find_default_int(List, Key) ->
  case lists:keyfind(Key, 1, List) of
    {Key, Val} when is_integer(Val) -> Val;
    {Key, Val} when is_float(Val) -> Val;
    {Key, Val} when is_binary(Val) -> binary_to_integer(Val);
    _ -> 0
  end.

find_default_int(List, Key, Default) ->
  case find_default_int(List, Key) of
    0 -> Default;
    T -> T
  end.

find_default_float(List, Key) ->
  case lists:keyfind(Key, 1, List) of
    {Key, Val} when is_integer(Val) -> erlang:float(Val);
    {Key, Val} when is_float(Val) -> Val;
    {Key, Val} when is_binary(Val) ->
      try list_to_float(binary_to_list(Val))
      catch
        error:badarg ->
          try erlang:float(list_to_integer(binary_to_list(Val)))
          catch
            error:badarg -> 0
          end
      end;
    _ -> 0
  end.

find_default_float(List, Key, Default) ->
  case find_default_float(List, Key) of
    0 -> Default;
    T -> T
  end.


find_default_list(List, Key) ->
  case lists:keyfind(Key, 1, List) of
    {Key, Val} when is_list(Val) -> Val;
    {Key, Val} when is_integer(Val) -> integer_to_list(Val);
    {Key, Val} when is_binary(Val) -> binary_to_list(Val);
    _ -> []
  end.

find_default_list(List, Key, Default) ->
  case find_default_int(List, Key) of
    [] -> Default;
    T -> T
  end.

-spec reform_local_date('YYYY-MM-DD' | 'Elastic') -> string().
reform_local_date('YYYY-MM-DD') ->

  {{Year, Month, Day}, _} = calendar:local_time(),
  DD = if Day < 10 ->
    "0" ++ integer_to_list(Day);
         true ->
           integer_to_list(Day)
       end,
  MM = if Month < 10 ->
    "0" ++ integer_to_list(Month);
         true ->
           integer_to_list(Month)
       end,
  integer_to_list(Year) ++ "-" ++ MM ++ "-" ++ DD;

reform_local_date('Elastic') ->
  Timezone = get_time_zone(),
  {{Year, Month, Day}, {Hour, Min, Sec}} = calendar:local_time(),
  Ss = if Sec < 10 ->
    "0" ++ integer_to_list(Sec);
         true ->
           integer_to_list(Sec)
       end,
  Mm = if Min < 10 ->
    "0" ++ integer_to_list(Min);
         true ->
           integer_to_list(Min)
       end,
  Hh = if Hour < 10 ->
    "0" ++ integer_to_list(Hour);
         true ->
           integer_to_list(Hour)
       end,
  DD = if Day < 10 ->
    "0" ++ integer_to_list(Day);
         true ->
           integer_to_list(Day)
       end,
  MM = if Month < 10 ->
    "0" ++ integer_to_list(Month);
         true ->
           integer_to_list(Month)
       end,
  integer_to_list(Year) ++ "-" ++ MM ++ "-" ++ DD
    ++ "T" ++ Hh ++ ":" ++ Mm ++ ":" ++ Ss ++ Timezone.

get_time_zone() ->
  U = calendar:universal_time(),
  L = calendar:local_time(),
  {_, {H, M, _}} = calendar:time_difference(U, L),

  "+" ++ integer_to_list(H) ++
    ":" ++ integer_to_list(M).