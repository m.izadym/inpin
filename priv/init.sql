CREATE DATABASE inpin;

use inpin;

create table agency
(
  id bigint auto_increment primary key,
  name  varchar(200) not null,
  parent_id bigint null
)
  charset = utf8;

create table agency_children
(
  agency_id    bigint  not null,
  childeren_id bigint not null
);

create table listing
(
  id    bigint auto_increment primary key,
  name      varchar(200)     not null,
  agency_id bigint           not null,
  latitude  double default 0 null,
  longitude double default 0 null
)
  charset = utf8;


