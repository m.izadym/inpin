-module(inpin_sql_connection).

-behaviour(gen_server).

%% API
-export([start/0, get_connection_Id/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(TIMEOUT, 1800000).

-record(state, {connection_pid :: pid, timer}).

%%%===================================================================
%%% API
%%%===================================================================

start() ->
  gen_server:start(?MODULE, [], []).

get_connection_Id(Pid) ->
  gen_server:call(Pid, get_connection_pid).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
init([]) ->
  Pid = connection(),
  Ref = timer:send_after(?TIMEOUT, renew_connection),
  {ok, #state{connection_pid = Pid, timer = Ref}}.

handle_call(get_connection_pid, _From, #state{connection_pid = Pid} = State) ->
  {reply, {ok, Pid}, State};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(renew_connection, #state{connection_pid = Pid}) ->
  exit(Pid, normal),
  Pid = connection(),
  Ref = timer:send_after(?TIMEOUT, renew_connection),
  {noreply, #state{connection_pid = Pid, timer = Ref}};


handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
connection() ->
  {ok, IP} = application:get_env(inpin, 'mysql.ip'),
  {ok, User} = application:get_env(inpin, 'mysql.user'),
  {ok, Password} = application:get_env(inpin, 'mysql.password'),
  {ok, Database} = application:get_env(inpin, 'mysql.database'),

  {ok, Pid} = mysql:start_link([{host, IP},
    {user, User},
    {password, Password},
    {database, Database}]),

  Pid.