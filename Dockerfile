FROM erlang:21.0

WORKDIR /buildroot

## Copy our Version
COPY release inpin

WORKDIR inpin

RUN tar -xvzf inpin-1.0.0.tar.gz

EXPOSE 3000

CMD ["/buildroot/inpin/bin/inpin", "foreground"]